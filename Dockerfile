# Use the official AlmaLinux image as the base image
FROM almalinux:latest

# Set environment variables
ENV USER=gitea \
    GITEA_CUSTOM=/var/lib/gitea/custom \
    GITEA_WORK_DIR=/var/lib/gitea \
    GITEA_LOG_ROOT_PATH=/var/lib/gitea/log

# Install dependencies
RUN dnf update -y && \
    dnf install -y git sqlite wget curl --allowerasing && \
    dnf clean all

# Create gitea user and necessary directories
RUN useradd -m -s /bin/bash -r $USER && \
    mkdir -p /var/lib/gitea/{custom,data,log} /etc/gitea && \
    chown -R $USER: /var/lib/gitea /etc/gitea && \
    chmod -R 750 /var/lib/gitea && \
    chmod 770 /etc/gitea

# Download Gitea binary
RUN wget -O /usr/local/bin/gitea https://dl.gitea.io/gitea/1.21.11/gitea-1.21.11-linux-amd64 && \
    chmod +x /usr/local/bin/gitea

# Set the user and workdir
USER $USER
WORKDIR /var/lib/gitea

# Expose HTTP port
EXPOSE 3000

# Start Gitea
ENTRYPOINT ["/usr/local/bin/gitea", "web"]
