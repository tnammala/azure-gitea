Documentation for Gitea

Files:
main.tf , Terraform
Dockerfile , Container with Gitea
Cloud-init , Automation

In collaboration with Alexander Lindholm

**Step 1**: Designing the infrastructure 

First step is to create a Terraform file, Terraform is used to create virtual machines.
The file was named "main.tf" and contains all the necessary code to properly set up a virtual machine on Azure. 
Note that to create resources on Azure, an account with a valid subscription is required, for this project, Azure for students was used as a subscription.

**Step 1.1**: Login as user to the VM

In this case, Windows 11 Powershell was used as the main terminal, any other terminal will be fine for the project. 

Opening up Powershell:
ssh user@vm.ip.address , will initiate the process of logging into the VM with the user associated with the VM. 
Since a password was used as authentication, it will prompt you to enter a password conncected to the user. After entering the password, it should log in. 

**Step 2**: Set up Nginx and SSL Certificate

While in the VM, run "sudo su" to swap over to the root user, this will allow you to bypass permissions and have full control over your system.

To set up the reverse proxy manager Nginx and set up a SSL Certificate, run these commands:
apt install nginx certbot python3-certbot-nginx -y
cp /etc/nginx/sites-available/default /etc/nginx/sites-available/cabin

It will download Nginx and Certbot.
Next step will be to configure Nginx to allow it to connect with your domain. 

vim /etc/nginx/sites-available/cabin , will open up a the file, it will be cluttured by a large amount of comments, to remove them, run this command: ":g/^\s*#/d"

Pressing the key "o" will allow you to edit the file, then make sure the file's contents look like this: 

server {
        listen 80 ;
        listen [::]:80 ;

        index index.html index.htm index.nginx-debian.html;

        server_name your.server.name(tnam.chas.dsnw.dev) www.your.server.name(tnam_chas_dsnw_dev);

        location / {
        proxy_pass http://your.ip.address:3000; (my ip is 52.178.137.14)
        }

Doing this will point Nginx to the appropriate destination, in this case it is the gitea container. 

ln -s /etc/nginx/sites-available/cabin /etc/nginx/sites-enabled/ , linking the cabin file to sites-enabled. 

**Step 2.1**:(Optional) Add test file 
mkdir /var/www/cabin , created a folder named cabin 

cd /var/www/cabin , change directory to that destination

vim index.html
<h1> test <h1> 

systemctl reload nginx

The steps above adds a test file that is used to verify that the domain works properly, if it does, you will see a blank page that only has "test" as text, when inputting the domain name into the web browser. 

**Step 2.2**: Add SSL Certificate using Certbot
certbot --nginx

crontab -e
1 1 1 * * certbot renew , renew the SSL Certificate the 1st of every month, such as January 1 and February 1, due to the SSL Certificate only lasting for two months, this will ensure it wont run out so your website can stay secured.

systemctl reload nginx

This should add a SSL Certificate to the domain so it receives the "https" and a lock so it becomes very secure. 






